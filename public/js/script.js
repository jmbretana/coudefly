$(document).ready(function () {
    "use strict";

    // Back to top button
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $(".back-to-top").fadeIn("slow");

            /*
            $(".headerClass").removeClass("bk-black");
            $(".headerClass").addClass("bk-white");

            $(".nav-link ").addClass("black");
            $(".nav-link ").removeClass("white");
            */
        } else {
            /*
            $(".headerClass").addClass("bk-black");
            $(".headerClass").removeClass("bk-white");

            $(".nav-link ").removeClass("black");
            $(".nav-link ").addClass("white");
            */

            $(".back-to-top").fadeOut("slow");
        }
    });

    $(".back-to-top").click(function () {
        $("html, body").animate({ scrollTop: 0 }, 1500, "easeInOutExpo");
        return false;
    });

    // Menu Navigation
    $(".nav-link").click(function () {
        $(".active").addClass("white");
        $(".active").removeClass("lightBlue");
        $(".active").removeClass("active");

        $(this).removeClass("white");
        $(this).addClass("lightBlue");
        $(this).addClass("active");

        let href = $(this).attr('href');

        $.animate({
            scrollTop: $(href).offset().top
        }, 2000);
        return false;
    });

    $("#enviarWhats").click(function (e) {
        var name = $("#name").val();
        var message = $("#message").val();

        var url =
            "https://api.whatsapp.com/send?phone=5491140445955" +
            "&text=Hola,%20mi%20nombre%20es:%20" + name + ",%20te%20contacto%20desde%20la%20web%20de%20Coudefly.%20Mensaje:%20" + message;

        window.open(url, "_blank");
    });
});
